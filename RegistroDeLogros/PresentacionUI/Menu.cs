﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PresentacionUI
{
    public partial class Menu : Form
    {
        public Menu()
        {
            InitializeComponent();
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        private void registrarLogrosToolStripMenuItem_Click(object sender, EventArgs e) // visualizar logros UI
        {
            
        }

        private void visualizarLogrosToolStripMenuItem_Click(object sender, EventArgs e) // visualizar Jugadores UI
        {
            VerLogros OFO = new VerLogros();
            OFO.MdiParent = this;
            OFO.Show();
        }

        private void agregarJugadorToolStripMenuItem_Click(object sender, EventArgs e) // Agregar Jugadores UI
        {
            AgregarJugadorDS OFO = new AgregarJugadorDS();
            OFO.MdiParent = this;
            OFO.Show();
        }

        private void agregarLogroToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AgregarLogros OFO = new AgregarLogros();
            OFO.MdiParent = this;
            OFO.Show();
        } // Agregar Logros UI

        private void plataformaToolStripMenuItem_Click(object sender, EventArgs e) // agregar Plataformas UI
        {
            AgregarPlataforma OFO = new AgregarPlataforma();
            OFO.MdiParent = this;
            OFO.Show();
        }

        private void logrosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ModificarDato OFO = new ModificarDato();
            OFO.MdiParent = this;
            OFO.Show();
        }

        private void logrosToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ModificarLogro OFO = new ModificarLogro();
            OFO.MdiParent = this;
            OFO.Show();
        }
    }
}
