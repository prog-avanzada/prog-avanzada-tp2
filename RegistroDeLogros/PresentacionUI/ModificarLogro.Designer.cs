﻿namespace PresentacionUI
{
    partial class ModificarLogro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.DATAgral = new System.Windows.Forms.DataGridView();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.DATAgral)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AccessibleDescription = "DATAgral";
            this.label1.AccessibleName = "DATAgral";
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(211, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(358, 16);
            this.label1.TabIndex = 3;
            this.label1.Text = "HACER CLICK SOBRE EL LOGRO QUE DESEA ELIMINAR";
            this.label1.Click += new System.EventHandler(this.DATAgral_Click);
            // 
            // DATAgral
            // 
            this.DATAgral.AccessibleDescription = "DATAgral";
            this.DATAgral.AccessibleName = "DATAgral";
            this.DATAgral.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DATAgral.Location = new System.Drawing.Point(147, 113);
            this.DATAgral.Name = "DATAgral";
            this.DATAgral.RowHeadersWidth = 51;
            this.DATAgral.RowTemplate.Height = 24;
            this.DATAgral.Size = new System.Drawing.Size(506, 276);
            this.DATAgral.TabIndex = 2;
            this.DATAgral.Click += new System.EventHandler(this.DATAgral_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(28, 113);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // ModificarLogro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.DATAgral);
            this.Name = "ModificarLogro";
            this.Text = "ModificarLogro";
            ((System.ComponentModel.ISupportInitialize)(this.DATAgral)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView DATAgral;
        private System.Windows.Forms.Button button1;
    }
}