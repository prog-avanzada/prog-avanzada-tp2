﻿using NegociosFunciones;
using Persistencia;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PresentacionUI
{

    public partial class ModificarLogro : Form
    {

        public Transacciones NServicios;
        public string SELECT;
        public Logro MiLogro;

        public ModificarLogro()
        {
            InitializeComponent();
            NServicios = new Transacciones();           
            MiLogro = new Logro();
            SELECT = string.Empty;
            DATAgral.DataSource = NServicios.VerTodosLosLogros();
        }

        private void DATAgral_Click(object sender, EventArgs e)
        {
          
            }
        private void DATAgral_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            
            // Verificar
            if (e.RowIndex >= 0)
            {
               

               
                

                // Obtener la fila seleccionada.
                DataGridViewRow filaSeleccionada = DATAgral.Rows[e.RowIndex];
                SELECT = filaSeleccionada.Cells["Logro"].Value.ToString();
                // Obtener el valor de la celda en la columna "nombre" (ajusta esto según tu estructura).
                MiLogro.LogroJuego = filaSeleccionada.Cells["Logro"].Value.ToString();
                MiLogro.NombreJuego = filaSeleccionada.Cells["NombreJuego"].Value.ToString();
                MiLogro.NombreJuego = filaSeleccionada.Cells["NombreJuego"].Value.ToString();
                MiLogro.Plataforma = filaSeleccionada.Cells["Plataforma"].Value.ToString();
                MiLogro.FechaDesbloqueo = Convert.ToDateTime(filaSeleccionada.Cells["Fecha"].Value);
                MiLogro.Puntaje = int.Parse(filaSeleccionada.Cells["Puntaje"].Value.ToString());
                
                // Mostrar los detalles del logro en los controles correspondientes.
                


            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DATAgral.DataSource = NServicios.ModificarLogro(MiLogro, SELECT); // modifica

            DATAgral.DataSource =  NServicios.VerTodosLosLogros();

            DATAgral.Refresh();
        }
    }
}
