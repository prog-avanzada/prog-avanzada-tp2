﻿namespace PresentacionUI
{
    partial class VerLogros
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TXTnombreJuego = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.DATAlogros = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.CBXplataformas = new System.Windows.Forms.ComboBox();
            this.button2 = new System.Windows.Forms.Button();
            this.botonPersonalizado1 = new PresentacionUI.BotonPersonalizado();
            ((System.ComponentModel.ISupportInitialize)(this.DATAlogros)).BeginInit();
            this.SuspendLayout();
            // 
            // TXTnombreJuego
            // 
            this.TXTnombreJuego.AccessibleDescription = "TXTnombreJuego";
            this.TXTnombreJuego.AccessibleName = "TXTnombreJuego";
            this.TXTnombreJuego.Location = new System.Drawing.Point(47, 64);
            this.TXTnombreJuego.Name = "TXTnombreJuego";
            this.TXTnombreJuego.Size = new System.Drawing.Size(127, 22);
            this.TXTnombreJuego.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(44, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(228, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "BUSCAR POR NOMBRE DE JUEGO";
            // 
            // button1
            // 
            this.button1.AccessibleDescription = "BTbuscar";
            this.button1.AccessibleName = "BTbuscar";
            this.button1.Location = new System.Drawing.Point(197, 64);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(92, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "BUSCAR";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // DATAlogros
            // 
            this.DATAlogros.AccessibleDescription = "DATAlogros";
            this.DATAlogros.AccessibleName = "DATAlogros";
            this.DATAlogros.AllowUserToOrderColumns = true;
            this.DATAlogros.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DATAlogros.Location = new System.Drawing.Point(12, 101);
            this.DATAlogros.Name = "DATAlogros";
            this.DATAlogros.ReadOnly = true;
            this.DATAlogros.RowHeadersWidth = 51;
            this.DATAlogros.RowTemplate.Height = 24;
            this.DATAlogros.Size = new System.Drawing.Size(836, 144);
            this.DATAlogros.TabIndex = 5;
            this.DATAlogros.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.ClickLogro);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(373, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(273, 16);
            this.label3.TabIndex = 6;
            this.label3.Text = "BUSCAR POR NOMBRE DE PLATAFORMA";
            // 
            // CBXplataformas
            // 
            this.CBXplataformas.AccessibleDescription = "CBXplataformas";
            this.CBXplataformas.AccessibleName = "CBXplataformas";
            this.CBXplataformas.FormattingEnabled = true;
            this.CBXplataformas.Location = new System.Drawing.Point(388, 59);
            this.CBXplataformas.Name = "CBXplataformas";
            this.CBXplataformas.Size = new System.Drawing.Size(121, 24);
            this.CBXplataformas.TabIndex = 7;
            // 
            // button2
            // 
            this.button2.AccessibleDescription = "BTbuscarP";
            this.button2.AccessibleName = "BTbuscarP";
            this.button2.Location = new System.Drawing.Point(538, 60);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(92, 23);
            this.button2.TabIndex = 8;
            this.button2.Text = "BUSCAR";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // botonPersonalizado1
            // 
            this.botonPersonalizado1.Location = new System.Drawing.Point(12, 289);
            this.botonPersonalizado1.Name = "botonPersonalizado1";
            this.botonPersonalizado1.Size = new System.Drawing.Size(359, 467);
            this.botonPersonalizado1.TabIndex = 9;
            // 
            // VerLogros
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(892, 768);
            this.Controls.Add(this.botonPersonalizado1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.CBXplataformas);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.DATAlogros);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TXTnombreJuego);
            this.Name = "VerLogros";
            this.Text = "VerLogros";
            ((System.ComponentModel.ISupportInitialize)(this.DATAlogros)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox TXTnombreJuego;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView DATAlogros;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox CBXplataformas;
        private System.Windows.Forms.Button button2;
        private BotonPersonalizado botonPersonalizado1;
    }
}