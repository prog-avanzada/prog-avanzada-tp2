﻿using NegociosFunciones;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PresentacionUI
{
    public partial class ModificarDato : Form
    {
        public Transacciones Nservicios;
        public ModificarDato()
        {
            InitializeComponent();
            Nservicios = new Transacciones();
            DATAgral.DataSource = Nservicios.VerTodosLosLogros();

        }

   

       public void DATAgral_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            // Verificar
            if (e.RowIndex >= 0)
            {
                // Obtener la fila seleccionada.
                DataGridViewRow filaSeleccionada = DATAgral.Rows[e.RowIndex];

                // Obtener el valor de la celda en la columna "nombre" (ajusta esto según tu estructura).
                string nombreLogro = filaSeleccionada.Cells["Logro"].Value.ToString();

                // Mostrar los detalles del logro en los controles correspondientes.
               Nservicios.EliminarLogro(nombreLogro); // se elimina la seleccionada
                DATAgral.DataSource = Nservicios.VerTodosLosLogros();

                DATAgral.Refresh();


            }
        }

        private void ModificarDato_Click(object sender, EventArgs e)
        {

        }
    }
}
