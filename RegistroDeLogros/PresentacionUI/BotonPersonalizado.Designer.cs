﻿namespace PresentacionUI
{
    partial class BotonPersonalizado
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.BTver = new System.Windows.Forms.Button();
            this.CBXopcion = new System.Windows.Forms.ComboBox();
            this.DATAinfo = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.DATAinfo)).BeginInit();
            this.SuspendLayout();
            // 
            // BTver
            // 
            this.BTver.AccessibleDescription = "BTver";
            this.BTver.AccessibleName = "BTver";
            this.BTver.Location = new System.Drawing.Point(44, 46);
            this.BTver.Name = "BTver";
            this.BTver.Size = new System.Drawing.Size(96, 28);
            this.BTver.TabIndex = 0;
            this.BTver.Text = "VER";
            this.BTver.UseVisualStyleBackColor = true;
            this.BTver.Click += new System.EventHandler(this.BTver_Click);
            // 
            // CBXopcion
            // 
            this.CBXopcion.AccessibleDescription = "CBXopcion";
            this.CBXopcion.AccessibleName = "CBXopcion";
            this.CBXopcion.FormattingEnabled = true;
            this.CBXopcion.Location = new System.Drawing.Point(179, 46);
            this.CBXopcion.Name = "CBXopcion";
            this.CBXopcion.Size = new System.Drawing.Size(121, 24);
            this.CBXopcion.TabIndex = 1;
            // 
            // DATAinfo
            // 
            this.DATAinfo.AccessibleDescription = "DATAinfo";
            this.DATAinfo.AccessibleName = "DATAinfo";
            this.DATAinfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DATAinfo.Location = new System.Drawing.Point(28, 121);
            this.DATAinfo.Name = "DATAinfo";
            this.DATAinfo.RowHeadersWidth = 51;
            this.DATAinfo.RowTemplate.Height = 24;
            this.DATAinfo.Size = new System.Drawing.Size(308, 329);
            this.DATAinfo.TabIndex = 3;
            // 
            // BotonPersonalizado
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.DATAinfo);
            this.Controls.Add(this.CBXopcion);
            this.Controls.Add(this.BTver);
            this.Name = "BotonPersonalizado";
            this.Size = new System.Drawing.Size(359, 474);
            ((System.ComponentModel.ISupportInitialize)(this.DATAinfo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button BTver;
        private System.Windows.Forms.ComboBox CBXopcion;
        private System.Windows.Forms.DataGridView DATAinfo;
    }
}
