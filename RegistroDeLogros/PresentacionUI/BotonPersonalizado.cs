﻿using NegociosFunciones;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PresentacionUI
{
    public partial class BotonPersonalizado : UserControl
    {

        public Transacciones Nservicios;
        List<string> opciones = new List<string>
            {
                "estadisticas"

            };

        public BotonPersonalizado()
        {
            InitializeComponent();

            Nservicios = new Transacciones();
           
            CBXopcion.Items.AddRange(opciones.ToArray());
        }

        private void BTver_Click(object sender, EventArgs e) // BOTON VER
        {

            if (CBXopcion.Text == "estadisticas")
            {
                DATAinfo.DataSource = Nservicios.Estadisticas();
                DATAinfo.Refresh();
            }


        }
    }
}
