﻿namespace PresentacionUI
{
    partial class ModificarDato
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DATAgral = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.DATAgral)).BeginInit();
            this.SuspendLayout();
            // 
            // DATAgral
            // 
            this.DATAgral.AccessibleDescription = "DATAgral";
            this.DATAgral.AccessibleName = "DATAgral";
            this.DATAgral.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DATAgral.Location = new System.Drawing.Point(191, 154);
            this.DATAgral.Name = "DATAgral";
            this.DATAgral.RowHeadersWidth = 51;
            this.DATAgral.RowTemplate.Height = 24;
            this.DATAgral.Size = new System.Drawing.Size(506, 276);
            this.DATAgral.TabIndex = 0;
            this.DATAgral.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DATAgral_CellClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(255, 102);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(358, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "HACER CLICK SOBRE EL LOGRO QUE DESEA ELIMINAR";
            // 
            // ModificarDato
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(986, 524);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.DATAgral);
            this.Name = "ModificarDato";
            this.Text = "ModificarDato";
            this.Click += new System.EventHandler(this.ModificarDato_Click);
            ((System.ComponentModel.ISupportInitialize)(this.DATAgral)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView DATAgral;
        private System.Windows.Forms.Label label1;
    }
}