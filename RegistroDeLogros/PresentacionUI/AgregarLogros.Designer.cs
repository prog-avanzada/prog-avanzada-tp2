﻿namespace PresentacionUI
{
    partial class AgregarLogros
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.INFO4 = new System.Windows.Forms.Label();
            this.TXTpuntaje = new System.Windows.Forms.TextBox();
            this.CBXListaJugadores = new System.Windows.Forms.ComboBox();
            this.INFO = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.INFO6 = new System.Windows.Forms.Label();
            this.INFO5 = new System.Windows.Forms.Label();
            this.INFO3 = new System.Windows.Forms.Label();
            this.INFO2 = new System.Windows.Forms.Label();
            this.CBXplataformas = new System.Windows.Forms.ComboBox();
            this.TXTfecha = new System.Windows.Forms.DateTimePicker();
            this.TXTlogro = new System.Windows.Forms.TextBox();
            this.TXTnombreJuego = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // INFO4
            // 
            this.INFO4.AccessibleDescription = "INFO4";
            this.INFO4.AccessibleName = "INFO4";
            this.INFO4.AutoSize = true;
            this.INFO4.Location = new System.Drawing.Point(98, 276);
            this.INFO4.Name = "INFO4";
            this.INFO4.Size = new System.Drawing.Size(52, 16);
            this.INFO4.TabIndex = 29;
            this.INFO4.Text = "Puntaje";
            // 
            // TXTpuntaje
            // 
            this.TXTpuntaje.AccessibleDescription = "TXTpuntaje";
            this.TXTpuntaje.AccessibleName = "TXTlogro";
            this.TXTpuntaje.Location = new System.Drawing.Point(185, 273);
            this.TXTpuntaje.Name = "TXTpuntaje";
            this.TXTpuntaje.Size = new System.Drawing.Size(100, 22);
            this.TXTpuntaje.TabIndex = 28;
            // 
            // CBXListaJugadores
            // 
            this.CBXListaJugadores.AccessibleDescription = "CBXListaJugadores";
            this.CBXListaJugadores.AccessibleName = "CBXListaJugadores";
            this.CBXListaJugadores.FormattingEnabled = true;
            this.CBXListaJugadores.Location = new System.Drawing.Point(185, 130);
            this.CBXListaJugadores.Name = "CBXListaJugadores";
            this.CBXListaJugadores.Size = new System.Drawing.Size(320, 24);
            this.CBXListaJugadores.TabIndex = 27;
            // 
            // INFO
            // 
            this.INFO.AccessibleDescription = "INFO";
            this.INFO.AccessibleName = "INFO";
            this.INFO.AutoSize = true;
            this.INFO.Location = new System.Drawing.Point(98, 133);
            this.INFO.Name = "INFO";
            this.INFO.Size = new System.Drawing.Size(57, 16);
            this.INFO.TabIndex = 26;
            this.INFO.Text = "Jugador";
            // 
            // button1
            // 
            this.button1.AccessibleDescription = "TXTlistadejugadores";
            this.button1.AccessibleName = "TXTlistadejugadores";
            this.button1.Location = new System.Drawing.Point(323, 66);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(143, 32);
            this.button1.TabIndex = 25;
            this.button1.Text = "REGISTRAR";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label5
            // 
            this.label5.AccessibleDescription = "TXTlistadejugadores";
            this.label5.AccessibleName = "TXTlistadejugadores";
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(64, 74);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(253, 16);
            this.label5.TabIndex = 24;
            this.label5.Text = "COMPLETE LOS CAMPOS Y PRESIONE";
            // 
            // INFO6
            // 
            this.INFO6.AccessibleDescription = "INFO6";
            this.INFO6.AccessibleName = "INFO6";
            this.INFO6.AutoSize = true;
            this.INFO6.Location = new System.Drawing.Point(94, 367);
            this.INFO6.Name = "INFO6";
            this.INFO6.Size = new System.Drawing.Size(45, 16);
            this.INFO6.TabIndex = 23;
            this.INFO6.Text = "Fecha";
            // 
            // INFO5
            // 
            this.INFO5.AccessibleDescription = "INFO5";
            this.INFO5.AccessibleName = "INFO5";
            this.INFO5.AutoSize = true;
            this.INFO5.Location = new System.Drawing.Point(94, 321);
            this.INFO5.Name = "INFO5";
            this.INFO5.Size = new System.Drawing.Size(72, 16);
            this.INFO5.TabIndex = 22;
            this.INFO5.Text = "Plataforma";
            // 
            // INFO3
            // 
            this.INFO3.AccessibleDescription = "INFO3";
            this.INFO3.AccessibleName = "INFO3";
            this.INFO3.AutoSize = true;
            this.INFO3.Location = new System.Drawing.Point(98, 226);
            this.INFO3.Name = "INFO3";
            this.INFO3.Size = new System.Drawing.Size(42, 16);
            this.INFO3.TabIndex = 21;
            this.INFO3.Text = "Logro";
            // 
            // INFO2
            // 
            this.INFO2.AccessibleDescription = "INFO2";
            this.INFO2.AccessibleName = "INFO2";
            this.INFO2.AutoSize = true;
            this.INFO2.Location = new System.Drawing.Point(50, 186);
            this.INFO2.Name = "INFO2";
            this.INFO2.Size = new System.Drawing.Size(116, 16);
            this.INFO2.TabIndex = 20;
            this.INFO2.Text = "Nombre de Juego";
            // 
            // CBXplataformas
            // 
            this.CBXplataformas.AccessibleDescription = "CBXplataformas";
            this.CBXplataformas.AccessibleName = "CBXplataformas";
            this.CBXplataformas.FormattingEnabled = true;
            this.CBXplataformas.Location = new System.Drawing.Point(185, 318);
            this.CBXplataformas.Name = "CBXplataformas";
            this.CBXplataformas.Size = new System.Drawing.Size(121, 24);
            this.CBXplataformas.TabIndex = 19;
            // 
            // TXTfecha
            // 
            this.TXTfecha.AccessibleDescription = "TXTfecha";
            this.TXTfecha.AccessibleName = "TXTfecha";
            this.TXTfecha.Location = new System.Drawing.Point(185, 361);
            this.TXTfecha.Name = "TXTfecha";
            this.TXTfecha.Size = new System.Drawing.Size(200, 22);
            this.TXTfecha.TabIndex = 18;
            // 
            // TXTlogro
            // 
            this.TXTlogro.AccessibleDescription = "TXTlogro";
            this.TXTlogro.AccessibleName = "TXTlogro";
            this.TXTlogro.Location = new System.Drawing.Point(185, 223);
            this.TXTlogro.Name = "TXTlogro";
            this.TXTlogro.Size = new System.Drawing.Size(100, 22);
            this.TXTlogro.TabIndex = 17;
            // 
            // TXTnombreJuego
            // 
            this.TXTnombreJuego.AccessibleDescription = "TXTnombreJuego";
            this.TXTnombreJuego.AccessibleName = "TXTnombreJuego";
            this.TXTnombreJuego.Location = new System.Drawing.Point(185, 180);
            this.TXTnombreJuego.Name = "TXTnombreJuego";
            this.TXTnombreJuego.Size = new System.Drawing.Size(100, 22);
            this.TXTnombreJuego.TabIndex = 16;
            // 
            // AgregarLogros
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.INFO4);
            this.Controls.Add(this.TXTpuntaje);
            this.Controls.Add(this.CBXListaJugadores);
            this.Controls.Add(this.INFO);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.INFO6);
            this.Controls.Add(this.INFO5);
            this.Controls.Add(this.INFO3);
            this.Controls.Add(this.INFO2);
            this.Controls.Add(this.CBXplataformas);
            this.Controls.Add(this.TXTfecha);
            this.Controls.Add(this.TXTlogro);
            this.Controls.Add(this.TXTnombreJuego);
            this.Name = "AgregarLogros";
            this.Text = "AgregarLogros";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label INFO4;
        private System.Windows.Forms.TextBox TXTpuntaje;
        private System.Windows.Forms.ComboBox CBXListaJugadores;
        private System.Windows.Forms.Label INFO;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label INFO6;
        private System.Windows.Forms.Label INFO5;
        private System.Windows.Forms.Label INFO3;
        private System.Windows.Forms.Label INFO2;
        private System.Windows.Forms.ComboBox CBXplataformas;
        private System.Windows.Forms.DateTimePicker TXTfecha;
        private System.Windows.Forms.TextBox TXTlogro;
        private System.Windows.Forms.TextBox TXTnombreJuego;
    }
}