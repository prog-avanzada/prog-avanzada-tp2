﻿using NegociosFunciones;
using Persistencia;
using PresentacionUI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;


namespace PresentacionUI
{
    public partial class AgregarLogros : Form
    {

        public SQLservicios SQLServ = new SQLservicios(); // servicios SQL
        public Transacciones NServicios = new Transacciones(); // servicios NEGOCIO

        public Logro MiLogro;

        private List<string> LPlataformas; // Lista de Plataformas para el Cbox

        private List<Jugador> MisJugadores; // lista de jugadores para el Cbox


        public AgregarLogros()
        {
            InitializeComponent();
            // instanciamos
            LPlataformas = new List<string>();
            MisJugadores = new List<Jugador>();
            MiLogro = new Logro();

            LPlataformas = NServicios.CargarPlataformas(LPlataformas); // cargamos plataformas a la lista
            CBXplataformas.Items.AddRange(LPlataformas.ToArray());  // cargamos la lista al Cbox

            MisJugadores = NServicios.CargarJugadores(MisJugadores); // cargamos plataformas a la lista
            var items = MisJugadores.Select(jugador => new { Display = $"{jugador.Nombre}", Value = jugador }).ToArray();

            // Agrega la lista de objetos anónimos al ComboBox
            CBXListaJugadores.Items.AddRange(items);

            // Establece las propiedades de DisplayMember y ValueMember
            CBXListaJugadores.DisplayMember = "Display";
            CBXListaJugadores.ValueMember = "Value";
        }

       



        private void button1_Click(object sender, EventArgs e) // agregar logro
        {
           
                     
            
            // verifico que no esten nulos los campos
            if (string.IsNullOrWhiteSpace(CBXListaJugadores.ToString()) ||
                string.IsNullOrWhiteSpace(TXTnombreJuego.Text) ||
                string.IsNullOrWhiteSpace(TXTlogro.Text) ||
                string.IsNullOrWhiteSpace(TXTpuntaje.Text) ||
                string.IsNullOrWhiteSpace(CBXplataformas.Text) ||
                string.IsNullOrWhiteSpace(TXTfecha.Value.ToString()))


            {
                MessageBox.Show("Todos los campos son obligatorios. Por favor, ingrese todos los datos.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Jugador jugadorSeleccionado = ((dynamic)CBXListaJugadores.SelectedItem).Value; // obtengo el obj value seleccionado y lo convierto 
            // Validar el formato del puntaje usando una expresión regular.
            if (!Regex.IsMatch(TXTpuntaje.Text, @"^\d+$"))
            {
                MessageBox.Show("El puntaje debe ser un valor numérico positivo.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            MiLogro.IDjugador = jugadorSeleccionado.Id;

            MiLogro.NombreJuego = TXTnombreJuego.Text;

            MiLogro.LogroJuego = TXTlogro.Text;

            MiLogro.Puntaje = Convert.ToInt32(TXTpuntaje.Text);

            MiLogro.Plataforma = CBXplataformas.Text;

            MiLogro.FechaDesbloqueo = TXTfecha.Value;

            if (SQLServ.AgregarLogro(MiLogro))
            {
                MessageBox.Show("Se agrego un nuevo logro");

            }
            else { MessageBox.Show("Error algo salio mal"); }
            
        }
    }
}
