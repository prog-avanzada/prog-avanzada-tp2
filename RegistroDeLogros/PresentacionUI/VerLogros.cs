﻿using NegociosFunciones;
using Persistencia;
using PresentacionUI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace PresentacionUI
{
    public partial class VerLogros : Form
    {
        private Transacciones Nservicios;// servicios NEGOCIO


        public BotonVer MiBoton;

        
        private List<string> LPlataformas; // Lista de Plataformas para el Cbox

        private List<Jugador> MisJugadores; // lista de jugadores para el Cbox

        public VerLogros()
        {
            InitializeComponent();
            Nservicios = new Transacciones();

            LPlataformas = new List<string>();
            MiBoton = new BotonVer();
            
          
            LPlataformas = Nservicios.CargarPlataformas(LPlataformas); // cargamos plataformas a la lista
            CBXplataformas.Items.AddRange(LPlataformas.ToArray());  // cargamos la lista al Cbox
           


        }

        private void button2_Click(object sender, EventArgs e) // Buscar por plataforma
        {

            DATAlogros.DataSource = Nservicios.BuscarxPlataforma(CBXplataformas.Text);
            DATAlogros.Refresh();
        }

        private void button1_Click(object sender, EventArgs e) // buscar por nombre
        {
            DataTable dt = new DataTable();
            dt = Nservicios.BuscarxNombre(TXTnombreJuego.Text);
            DATAlogros.DataSource = dt;
            DATAlogros.Refresh();
        }

        private void ClickLogro(object sender, DataGridViewCellEventArgs e)
        {

            // Verificar
            if (e.RowIndex >= 0)
            {
                // Obtener la fila seleccionada.
                DataGridViewRow filaSeleccionada = DATAlogros.Rows[e.RowIndex];

                // Obtener el valor de la celda en la columna "nombre" (ajusta esto según tu estructura).
                string nombreLogro = filaSeleccionada.Cells["Logro"].Value.ToString();

                // Mostrar los detalles del logro en los controles correspondientes.
                DATAlogros.DataSource = Nservicios.Detalles(nombreLogro);
                DATAlogros.Refresh();


            }
        }

        private void BotonPerso_Click(object sender, EventArgs e) // BOTON PERSONALIZADO
        {
    

           
        }
    }
}






