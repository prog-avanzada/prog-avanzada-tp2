﻿namespace PresentacionUI
{
    partial class Menu
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.archivoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarLogroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.visualizarLogrosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.jugadoresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.agregarJugadorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.agregarLogroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.plataformaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mofificarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eliminarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logrosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logrosToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.archivoToolStripMenuItem,
            this.registrarLogroToolStripMenuItem,
            this.jugadoresToolStripMenuItem,
            this.mofificarToolStripMenuItem,
            this.eliminarToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1151, 28);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // archivoToolStripMenuItem
            // 
            this.archivoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.salirToolStripMenuItem});
            this.archivoToolStripMenuItem.Name = "archivoToolStripMenuItem";
            this.archivoToolStripMenuItem.Size = new System.Drawing.Size(73, 24);
            this.archivoToolStripMenuItem.Text = "Archivo";
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            this.salirToolStripMenuItem.Size = new System.Drawing.Size(121, 26);
            this.salirToolStripMenuItem.Text = "Salir";
            this.salirToolStripMenuItem.Click += new System.EventHandler(this.salirToolStripMenuItem_Click);
            // 
            // registrarLogroToolStripMenuItem
            // 
            this.registrarLogroToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.visualizarLogrosToolStripMenuItem});
            this.registrarLogroToolStripMenuItem.Name = "registrarLogroToolStripMenuItem";
            this.registrarLogroToolStripMenuItem.Size = new System.Drawing.Size(86, 24);
            this.registrarLogroToolStripMenuItem.Text = "Visualizar";
            // 
            // visualizarLogrosToolStripMenuItem
            // 
            this.visualizarLogrosToolStripMenuItem.Name = "visualizarLogrosToolStripMenuItem";
            this.visualizarLogrosToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.visualizarLogrosToolStripMenuItem.Text = "Logros";
            this.visualizarLogrosToolStripMenuItem.Click += new System.EventHandler(this.visualizarLogrosToolStripMenuItem_Click);
            // 
            // jugadoresToolStripMenuItem
            // 
            this.jugadoresToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.agregarJugadorToolStripMenuItem,
            this.agregarLogroToolStripMenuItem,
            this.plataformaToolStripMenuItem});
            this.jugadoresToolStripMenuItem.Name = "jugadoresToolStripMenuItem";
            this.jugadoresToolStripMenuItem.Size = new System.Drawing.Size(77, 24);
            this.jugadoresToolStripMenuItem.Text = "Agregar";
            // 
            // agregarJugadorToolStripMenuItem
            // 
            this.agregarJugadorToolStripMenuItem.Name = "agregarJugadorToolStripMenuItem";
            this.agregarJugadorToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.agregarJugadorToolStripMenuItem.Text = "Jugador";
            this.agregarJugadorToolStripMenuItem.Click += new System.EventHandler(this.agregarJugadorToolStripMenuItem_Click);
            // 
            // agregarLogroToolStripMenuItem
            // 
            this.agregarLogroToolStripMenuItem.Name = "agregarLogroToolStripMenuItem";
            this.agregarLogroToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.agregarLogroToolStripMenuItem.Text = "Logro";
            this.agregarLogroToolStripMenuItem.Click += new System.EventHandler(this.agregarLogroToolStripMenuItem_Click);
            // 
            // plataformaToolStripMenuItem
            // 
            this.plataformaToolStripMenuItem.Name = "plataformaToolStripMenuItem";
            this.plataformaToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.plataformaToolStripMenuItem.Text = "Plataforma";
            this.plataformaToolStripMenuItem.Click += new System.EventHandler(this.plataformaToolStripMenuItem_Click);
            // 
            // mofificarToolStripMenuItem
            // 
            this.mofificarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.logrosToolStripMenuItem1});
            this.mofificarToolStripMenuItem.Name = "mofificarToolStripMenuItem";
            this.mofificarToolStripMenuItem.Size = new System.Drawing.Size(83, 24);
            this.mofificarToolStripMenuItem.Text = "Mofificar";
            // 
            // eliminarToolStripMenuItem
            // 
            this.eliminarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.logrosToolStripMenuItem});
            this.eliminarToolStripMenuItem.Name = "eliminarToolStripMenuItem";
            this.eliminarToolStripMenuItem.Size = new System.Drawing.Size(77, 24);
            this.eliminarToolStripMenuItem.Text = "Eliminar";
            // 
            // logrosToolStripMenuItem
            // 
            this.logrosToolStripMenuItem.Name = "logrosToolStripMenuItem";
            this.logrosToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.logrosToolStripMenuItem.Text = "Logros";
            this.logrosToolStripMenuItem.Click += new System.EventHandler(this.logrosToolStripMenuItem_Click);
            // 
            // logrosToolStripMenuItem1
            // 
            this.logrosToolStripMenuItem1.Name = "logrosToolStripMenuItem1";
            this.logrosToolStripMenuItem1.Size = new System.Drawing.Size(224, 26);
            this.logrosToolStripMenuItem1.Text = "Logros";
            this.logrosToolStripMenuItem1.Click += new System.EventHandler(this.logrosToolStripMenuItem1_Click);
            // 
            // Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1151, 543);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Menu";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem archivoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarLogroToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem visualizarLogrosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem jugadoresToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem agregarJugadorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem agregarLogroToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem plataformaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mofificarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eliminarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logrosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logrosToolStripMenuItem1;
    }
}

