﻿using NegociosFunciones;
using Persistencia;
using PresentacionUI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PresentacionUI
{
    public partial class AgregarJugadorDS : Form
    {
        public Jugador NuevoJugador;
        public SQLservicios SQLservicios;

        public AgregarJugadorDS()
        {
            InitializeComponent();

        }

        private void button1_Click(object sender, EventArgs e) // BOTON AGREGAR
        {
            NuevoJugador = new Jugador();
            SQLservicios = new SQLservicios();

           

            if (string.IsNullOrWhiteSpace(TXTnombreJugador.Text))
            {
                MessageBox.Show("Todos los campos son obligatorios. Por favor, ingrese todos los datos.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;

            }
           
            NuevoJugador.Nombre = TXTnombreJugador.Text;
            NuevoJugador.PuntajeTotal = 0;
            NuevoJugador.LogrosDesbloqueados = 0;
            
            if (SQLservicios.AgregarJugador(NuevoJugador))
            {
                MessageBox.Show("Se agrego un nuevo jugador");

            }
            else { MessageBox.Show("Error algo salio mal"); }
            ;
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void TXTnombreJugador_TextChanged(object sender, EventArgs e)
        {

        }
    }
}

