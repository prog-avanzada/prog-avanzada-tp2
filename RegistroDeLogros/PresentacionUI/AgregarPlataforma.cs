﻿using NegociosFunciones;
using Persistencia;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PresentacionUI
{
    public partial class AgregarPlataforma : Form
    {
        public Transacciones ServNegocios;
        public AgregarPlataforma()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)//  Boton agregar plataforma
        {
            ServNegocios = new Transacciones();
            
            if (string.IsNullOrWhiteSpace(TXTnombrePlataforma.Text))
            {
                MessageBox.Show("Todos los campos son obligatorios. Por favor, ingrese todos los datos.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;

            }

            
            if (ServNegocios.AgregarPlataforma(TXTnombrePlataforma.Text))
            {
                MessageBox.Show("Se agrego un nueva plataforma");

            }
            else { MessageBox.Show("Error algo salio mal"); }
        }
    }
}




                                                                               // Validar el formato del puntaje usando una expresión regular.

