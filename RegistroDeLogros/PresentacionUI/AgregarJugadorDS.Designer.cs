﻿namespace PresentacionUI
{
    partial class AgregarJugadorDS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.TXTnombreJugador = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(271, 37);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(133, 23);
            this.button1.TabIndex = 22;
            this.button1.Text = "AGREGAR";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(28, 40);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(237, 16);
            this.label5.TabIndex = 21;
            this.label5.Text = "COMPLETE EL CAMPO Y PRESIONE:";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(34, 90);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(134, 16);
            this.label1.TabIndex = 17;
            this.label1.Text = "Nombre del Jugador:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // TXTnombreJugador
            // 
            this.TXTnombreJugador.AccessibleDescription = "TXTnombreJugador";
            this.TXTnombreJugador.AccessibleName = "TXTnombreJugador";
            this.TXTnombreJugador.Location = new System.Drawing.Point(174, 90);
            this.TXTnombreJugador.Name = "TXTnombreJugador";
            this.TXTnombreJugador.Size = new System.Drawing.Size(151, 22);
            this.TXTnombreJugador.TabIndex = 13;
            this.TXTnombreJugador.TextChanged += new System.EventHandler(this.TXTnombreJugador_TextChanged);
            // 
            // AgregarJugadorDS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(458, 173);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TXTnombreJugador);
            this.Name = "AgregarJugadorDS";
            this.Text = "AgregarJugadorDS";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TXTnombreJugador;
    }
}