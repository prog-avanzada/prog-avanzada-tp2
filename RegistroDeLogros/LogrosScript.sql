USE [master]
GO
/****** Object:  Database [LOGROS]    Script Date: 15/11/2023 13:03:37 ******/
CREATE DATABASE [LOGROS]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'LOGROS', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL16.MSSQLSERVER\MSSQL\DATA\LOGROS.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'LOGROS_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL16.MSSQLSERVER\MSSQL\DATA\LOGROS_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT, LEDGER = OFF
GO
ALTER DATABASE [LOGROS] SET COMPATIBILITY_LEVEL = 160
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [LOGROS].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [LOGROS] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [LOGROS] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [LOGROS] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [LOGROS] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [LOGROS] SET ARITHABORT OFF 
GO
ALTER DATABASE [LOGROS] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [LOGROS] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [LOGROS] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [LOGROS] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [LOGROS] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [LOGROS] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [LOGROS] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [LOGROS] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [LOGROS] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [LOGROS] SET  DISABLE_BROKER 
GO
ALTER DATABASE [LOGROS] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [LOGROS] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [LOGROS] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [LOGROS] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [LOGROS] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [LOGROS] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [LOGROS] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [LOGROS] SET RECOVERY FULL 
GO
ALTER DATABASE [LOGROS] SET  MULTI_USER 
GO
ALTER DATABASE [LOGROS] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [LOGROS] SET DB_CHAINING OFF 
GO
ALTER DATABASE [LOGROS] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [LOGROS] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [LOGROS] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [LOGROS] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'LOGROS', N'ON'
GO
ALTER DATABASE [LOGROS] SET QUERY_STORE = ON
GO
ALTER DATABASE [LOGROS] SET QUERY_STORE (OPERATION_MODE = READ_WRITE, CLEANUP_POLICY = (STALE_QUERY_THRESHOLD_DAYS = 30), DATA_FLUSH_INTERVAL_SECONDS = 900, INTERVAL_LENGTH_MINUTES = 60, MAX_STORAGE_SIZE_MB = 1000, QUERY_CAPTURE_MODE = AUTO, SIZE_BASED_CLEANUP_MODE = AUTO, MAX_PLANS_PER_QUERY = 200, WAIT_STATS_CAPTURE_MODE = ON)
GO
USE [LOGROS]
GO
/****** Object:  Table [dbo].[Jugador]    Script Date: 15/11/2023 13:03:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Jugador](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NombreJugador] [varchar](50) NULL,
	[PuntajeTotal] [int] NULL,
	[LogrosDesbloqueados] [int] NULL,
 CONSTRAINT [PK_Jugador] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ListaLogros]    Script Date: 15/11/2023 13:03:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ListaLogros](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Logro] [varchar](50) NULL,
	[NombreJuego] [varchar](50) NULL,
	[Plataforma] [varchar](50) NULL,
	[Puntaje] [int] NULL,
	[IDjugador] [int] NULL,
	[Fecha] [datetime] NULL,
 CONSTRAINT [PK_ListaLogros] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ListaPlataformas]    Script Date: 15/11/2023 13:03:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ListaPlataformas](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Plataforma] [varchar](50) NULL,
 CONSTRAINT [PK_ListaPlataformas] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Jugador] ON 

INSERT [dbo].[Jugador] ([Id], [NombreJugador], [PuntajeTotal], [LogrosDesbloqueados]) VALUES (1, N'"Marcos"', 0, NULL)
INSERT [dbo].[Jugador] ([Id], [NombreJugador], [PuntajeTotal], [LogrosDesbloqueados]) VALUES (2, N'"Luciana"', 0, NULL)
INSERT [dbo].[Jugador] ([Id], [NombreJugador], [PuntajeTotal], [LogrosDesbloqueados]) VALUES (3, N'"Samuel"', 0, NULL)
INSERT [dbo].[Jugador] ([Id], [NombreJugador], [PuntajeTotal], [LogrosDesbloqueados]) VALUES (4, N'Maxi', 0, 0)
INSERT [dbo].[Jugador] ([Id], [NombreJugador], [PuntajeTotal], [LogrosDesbloqueados]) VALUES (5, N'Sandra', 0, 0)
INSERT [dbo].[Jugador] ([Id], [NombreJugador], [PuntajeTotal], [LogrosDesbloqueados]) VALUES (6, N'Luis', 0, 0)
INSERT [dbo].[Jugador] ([Id], [NombreJugador], [PuntajeTotal], [LogrosDesbloqueados]) VALUES (7, N'Jose', 0, 0)
INSERT [dbo].[Jugador] ([Id], [NombreJugador], [PuntajeTotal], [LogrosDesbloqueados]) VALUES (8, N'', 0, 0)
INSERT [dbo].[Jugador] ([Id], [NombreJugador], [PuntajeTotal], [LogrosDesbloqueados]) VALUES (9, N'thiago', 0, 0)
SET IDENTITY_INSERT [dbo].[Jugador] OFF
GO
SET IDENTITY_INSERT [dbo].[ListaLogros] ON 

INSERT [dbo].[ListaLogros] ([Id], [Logro], [NombreJuego], [Plataforma], [Puntaje], [IDjugador], [Fecha]) VALUES (2, N'PRUEBA', N'PRUEBA', N'PRUEBA', 5, NULL, NULL)
INSERT [dbo].[ListaLogros] ([Id], [Logro], [NombreJuego], [Plataforma], [Puntaje], [IDjugador], [Fecha]) VALUES (6, N'RAPIX10', N'SONIC', N'PS1', 10, 1, CAST(N'2023-10-12T22:49:57.000' AS DateTime))
INSERT [dbo].[ListaLogros] ([Id], [Logro], [NombreJuego], [Plataforma], [Puntaje], [IDjugador], [Fecha]) VALUES (8, N'Survivor', N'Resident Evil', N'playstation', 10, 6, CAST(N'2002-07-20T03:24:29.000' AS DateTime))
INSERT [dbo].[ListaLogros] ([Id], [Logro], [NombreJuego], [Plataforma], [Puntaje], [IDjugador], [Fecha]) VALUES (10, N'x', N'x', N'x', 1, 2, CAST(N'2000-10-10T00:00:00.000' AS DateTime))
SET IDENTITY_INSERT [dbo].[ListaLogros] OFF
GO
SET IDENTITY_INSERT [dbo].[ListaPlataformas] ON 

INSERT [dbo].[ListaPlataformas] ([Id], [Plataforma]) VALUES (1, N'playstation')
INSERT [dbo].[ListaPlataformas] ([Id], [Plataforma]) VALUES (2, N'playstation 2')
INSERT [dbo].[ListaPlataformas] ([Id], [Plataforma]) VALUES (3, N'playstation 3')
INSERT [dbo].[ListaPlataformas] ([Id], [Plataforma]) VALUES (4, N'playstation 4')
INSERT [dbo].[ListaPlataformas] ([Id], [Plataforma]) VALUES (5, N'playstation 5')
INSERT [dbo].[ListaPlataformas] ([Id], [Plataforma]) VALUES (6, N'Xbox One')
INSERT [dbo].[ListaPlataformas] ([Id], [Plataforma]) VALUES (7, N'Xbox One S')
INSERT [dbo].[ListaPlataformas] ([Id], [Plataforma]) VALUES (8, N'Xbox One X')
INSERT [dbo].[ListaPlataformas] ([Id], [Plataforma]) VALUES (9, N'PC')
INSERT [dbo].[ListaPlataformas] ([Id], [Plataforma]) VALUES (10, N'Nintendo 64')
INSERT [dbo].[ListaPlataformas] ([Id], [Plataforma]) VALUES (11, N'Nintendo GameCube')
INSERT [dbo].[ListaPlataformas] ([Id], [Plataforma]) VALUES (12, N'Nintendo DS')
INSERT [dbo].[ListaPlataformas] ([Id], [Plataforma]) VALUES (13, N'Nintendo 3DS')
INSERT [dbo].[ListaPlataformas] ([Id], [Plataforma]) VALUES (14, N'Nintendo 2DS')
INSERT [dbo].[ListaPlataformas] ([Id], [Plataforma]) VALUES (15, N'Nintendo Wii')
INSERT [dbo].[ListaPlataformas] ([Id], [Plataforma]) VALUES (16, N'')
INSERT [dbo].[ListaPlataformas] ([Id], [Plataforma]) VALUES (17, N'ios')
INSERT [dbo].[ListaPlataformas] ([Id], [Plataforma]) VALUES (18, N'gameboy')
SET IDENTITY_INSERT [dbo].[ListaPlataformas] OFF
GO
ALTER TABLE [dbo].[ListaLogros]  WITH CHECK ADD  CONSTRAINT [FK_ListaLogros_Jugador] FOREIGN KEY([IDjugador])
REFERENCES [dbo].[Jugador] ([Id])
GO
ALTER TABLE [dbo].[ListaLogros] CHECK CONSTRAINT [FK_ListaLogros_Jugador]
GO
USE [master]
GO
ALTER DATABASE [LOGROS] SET  READ_WRITE 
GO
