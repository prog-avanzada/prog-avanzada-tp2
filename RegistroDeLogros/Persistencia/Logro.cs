﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistencia
{
    public class Logro
    {
        public int IDjugador {  get; set; }
        public string NombreJuego {  get; set; }
        public string LogroJuego {  get; set; }
        public string Plataforma { get; set; }
        public int Puntaje {  get; set; }

        public DateTime FechaDesbloqueo { get; set; }

        //public string FechaFormateada { get; set; }
    }
}
