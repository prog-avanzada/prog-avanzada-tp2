﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistencia
{
        public class Jugador
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public int LogrosDesbloqueados {  get; set; }   
        public int PuntajeTotal {  get; set; }
    }
}
