﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic;
using System.Windows;
using System.Windows.Forms;


namespace Persistencia
{
    // cadena de conexion: Data Source=.;Initial Catalog=LOGROS;Integrated Security=True
    // consulta sql agregar jugador: INSERT INTO Jugador(Id,NombreJugador,PuntajeTotal) VALUES (1 ,'"Marcos"', 0 );
    // ('"+Nlogro.LogroJuego+"','"+Nlogro.NombreJuego+"','"+Nlogro.Plataforma+"',"+Nlogro.Puntaje+ "," +Nlogro.IDjugador+")"

    public class SQLservicios
    {




        private string cadenaConexion = "Data Source=.;Initial Catalog=LOGROS;Integrated Security=True"; // cadena de conexión.


        public SQLservicios()
        {

        }


        public bool AgregarLogro(Logro Nlogro)
        {
            using (SqlConnection conexion = new SqlConnection(cadenaConexion))
            {
                string consulta = "INSERT INTO ListaLogros (Logro,NombreJuego,Plataforma,Puntaje,IDjugador,Fecha) " +
                        "VALUES (@LOGRO,@JUEGO,@PLATAFORMA,@PUNTAJE,@IDJUGADOR,@FECHA)";


                using (SqlCommand comando = new SqlCommand(consulta, conexion))
                {
                    // asignacion de valores a los parametros que voy a usar para la consulta

                    comando.Parameters.AddWithValue("@LOGRO", Nlogro.LogroJuego);
                    comando.Parameters.AddWithValue("@JUEGO", Nlogro.NombreJuego);
                    comando.Parameters.AddWithValue("@PLATAFORMA", Nlogro.Plataforma);
                    comando.Parameters.AddWithValue("@PUNTAJE", Nlogro.Puntaje);
                    comando.Parameters.AddWithValue("@IDJUGADOR", Nlogro.IDjugador);
                    comando.Parameters.AddWithValue("@FECHA", Nlogro.FechaDesbloqueo);


                    try
                    {
                        conexion.Open(); // abrir conexion
                        comando.ExecuteNonQuery(); // realizar consulta
                        conexion.Close();// cerrar conexion
                        return true; // si se realiza la consulta devuelve true
                    }

                    catch (Exception ex)
                    {
                        // Manejar errores de conexión o SQL aquí.
                        return false;
                    }
                }

            }

        }

        public bool AgregarPlataforma(String Plataforma)
        {
            // Crear una conexión a la base de datos
            using (SqlConnection conexion = new SqlConnection(cadenaConexion))
            {
                // Establecer la consulta SQL
                string consulta = "INSERT INTO ListaPlataformas(Plataforma) VALUES (@PLATAFORMA)";

                // Crear un comando para ejecutar la consulta
                using (SqlCommand comando = new SqlCommand(consulta, conexion))
                {

                    comando.Parameters.AddWithValue("@PLATAFORMA", Plataforma);


                    // Ejecutar la consulta y obtener un lector de datos
                    try
                    {
                        conexion.Open(); // abrir conexion
                        comando.ExecuteNonQuery(); // realizar consulta
                        conexion.Close();// cerrar conexion
                        return true; // si se realiza la consulta devuelve true


                    }

                    catch (Exception ex)
                    {
                        // Manejar errores de conexión o SQL aquí.
                        return false;
                    }

                }
            }
        }
        public bool AgregarJugador(Jugador NuevoJugador)
        {
            // Crear una conexión a la base de datos
            using (SqlConnection conexion = new SqlConnection(cadenaConexion))
            {
                // Establecer la consulta SQL
                string consulta = "INSERT INTO Jugador (NombreJugador,PuntajeTotal, LogrosDesbloqueados) VALUES (@NOMBRE,@PUNTAJE,@LOGROS)";

                // Crear un comando para ejecutar la consulta
                using (SqlCommand comando = new SqlCommand(consulta, conexion))
                {

                    comando.Parameters.AddWithValue("@NOMBRE", NuevoJugador.Nombre);
                    comando.Parameters.AddWithValue("@PUNTAJE", NuevoJugador.PuntajeTotal);
                    comando.Parameters.AddWithValue("@LOGROS", NuevoJugador.LogrosDesbloqueados);

                    // Ejecutar la consulta y obtener un lector de datos
                    try
                    {
                        conexion.Open(); // abrir conexion
                        comando.ExecuteNonQuery(); // realizar consulta
                        conexion.Close();// cerrar conexion
                        return true; // si se realiza la consulta devuelve true


                    }

                    catch (Exception ex)
                    {
                        // Manejar errores de conexión o SQL aquí.
                        return false;
                    }

                }
            }
        }

        public List<Jugador> LlenarListaJugadores(List<Jugador> ListaJugadores)
        {
            try
            {
                using (SqlConnection conexion = new SqlConnection(cadenaConexion))
                {
                    string consulta = "SELECT Id,NombreJugador,PuntajeTotal,LogrosDesbloqueados FROM Jugador";

                    using (SqlCommand comando = new SqlCommand(consulta, conexion))
                    {
                        conexion.Open();

                        using (SqlDataReader reader = comando.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Jugador jugador = new Jugador
                                {
                                    Id = Convert.ToInt32(reader["Id"]),
                                    Nombre = Convert.ToString(reader["NombreJugador"]),
                                    PuntajeTotal = reader["PuntajeTotal"] != DBNull.Value ? Convert.ToInt32(reader["PuntajeTotal"]) : 0,
                                    LogrosDesbloqueados = reader["LogrosDesbloqueados"] != DBNull.Value ? Convert.ToInt32(reader["LogrosDesbloqueados"]) : 0
                                    // Otros atributos según la estructura de tu tabla
                                };

                                ListaJugadores.Add(jugador);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // Manejar la excepción según tus necesidades
                Console.WriteLine("Error al llenar la lista de jugadores: " + ex.Message);
            }

            return ListaJugadores;
        }

        public List<string> LlenarListaPlataformas(List<string> ListaPlataformas)
        {
            try
            {
                using (SqlConnection conexion = new SqlConnection(cadenaConexion))
                {
                    string consulta = "SELECT * FROM ListaPlataformas";

                    using (SqlCommand comando = new SqlCommand(consulta, conexion))
                    {
                        conexion.Open();

                        using (SqlDataReader reader = comando.ExecuteReader())
                        {
                            while (reader.Read())
                            {

                                {

                                    string plataforma = reader["Plataforma"].ToString();
                                    ListaPlataformas.Add(plataforma);
                                };


                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // Manejar la excepción según tus necesidades
                Console.WriteLine("Error al llenar la lista de jugadores: " + ex.Message);
            }

            return ListaPlataformas;
        }

        public DataTable VerEstadisticas()
        {
            using (SqlConnection conexion = new SqlConnection(cadenaConexion))
            {
                string consulta = "SELECT jugador.NombreJugador AS NombreJugador, COUNT(ListaLogros.Id) AS LogrosDesbloqueados,SUM(ListaLogros.puntaje) AS PuntajeTotal FROM Jugador LEFT JOIN  ListaLogros ON jugador.id = ListaLogros.IDjugador GROUP BY Jugador.Id, Jugador.NombreJugador ORDER BY PuntajeTotal DESC";
                conexion.Open();

                using (SqlCommand command = new SqlCommand(consulta, conexion))
                {


                    SqlDataAdapter adaptador = new SqlDataAdapter(command);
                    DataTable resultado = new DataTable();

                    try
                    {
                        adaptador.Fill(resultado); // llenamos la tabla con el resultado de la consulta 
                        conexion.Close();
                        return resultado; // retornamos una tabla con el resultado adjunto

                    }
                    catch (Exception ex)
                    {
                        // Manejar errores de conexión o SQL aquí.
                        conexion.Close();
                        return null;

                    }
                }

            }
        }

        public DataTable BuscarPorPlataforma(string campo)
        {

            DataTable Resulta = new DataTable();

            using (SqlConnection conexion = new SqlConnection(cadenaConexion))
            {
                string consulta = "SELECT Plataforma, Logro FROM ListaLogros WHERE Plataforma = @Plataforma";
                conexion.Open();

                using (SqlCommand command = new SqlCommand(consulta, conexion))
                {
                    command.Parameters.AddWithValue("@Plataforma", campo);

                    SqlDataAdapter adaptador = new SqlDataAdapter(command);


                    try
                    {
                        adaptador.Fill(Resulta); // llenamos la tabla con el resultado de la consulta 
                        conexion.Close();
                        return Resulta; // retornamos una tabla con el resultado adjunto

                    }
                    catch (Exception ex)
                    {
                        // Manejar errores de conexión o SQL aquí.
                        conexion.Close();
                        return null;

                    }
                }

            }
        }

        public DataTable VerTodosLogros()
        {

            DataTable Resulta = new DataTable();

            using (SqlConnection conexion = new SqlConnection(cadenaConexion))
            {
                string consulta = "SELECT   Jugador.NombreJugador AS NombreJugador, ListaLogros.Logro,ListaLogros.Puntaje, ListaLogros.NombreJuego,ListaLogros.Plataforma, ListaLogros.Fecha      " +
                    "FROM   Jugador JOIN    ListaLogros ON Jugador.Id =   ListaLogros.IdJugador";
                conexion.Open();

                using (SqlCommand command = new SqlCommand(consulta, conexion))
                {

                    SqlDataAdapter adaptador = new SqlDataAdapter(command);


                    try
                    {
                        adaptador.Fill(Resulta); // llenamos la tabla con el resultado de la consulta 
                        conexion.Close();
                        return Resulta; // retornamos una tabla con el resultado adjunto

                    }
                    catch (Exception ex)
                    {
                        // Manejar errores de conexión o SQL aquí.
                        conexion.Close();
                        return null;

                    }
                }

            }
        }

        public DataTable BuscarPorNombre(string campo)
        {

            DataTable Resulta = new DataTable();

            using (SqlConnection conexion = new SqlConnection(cadenaConexion))
            {
                string consulta = "SELECT NombreJuego, Logro FROM ListaLogros WHERE NombreJuego LIKE @NOMBRE";
                conexion.Open();

                using (SqlCommand command = new SqlCommand(consulta, conexion))
                {
                    command.Parameters.AddWithValue("@NOMBRE", campo + "%");

                    SqlDataAdapter adaptador = new SqlDataAdapter(command);


                    try
                    {
                        adaptador.Fill(Resulta); // llenamos la tabla con el resultado de la consulta 
                        conexion.Close();
                        return Resulta; // retornamos una tabla con el resultado adjunto

                    }
                    catch (Exception ex)
                    {
                        // Manejar errores de conexión o SQL aquí.
                        conexion.Close();
                        return null;

                    }
                }

            }
        }

        public DataTable MostrarDetallesLogro(string logro)

        {
            using (SqlConnection conexion = new SqlConnection(cadenaConexion))
            {
                string consulta = "SELECT * FROM ListaLogros WHERE Logro = @LOGRO";
                conexion.Open();

                using (SqlCommand command = new SqlCommand(consulta, conexion))
                {
                    command.Parameters.AddWithValue("@LOGRO", logro);

                    SqlDataAdapter adaptador = new SqlDataAdapter(command);
                    DataTable resultado = new DataTable();

                    try
                    {
                        adaptador.Fill(resultado); // llenamos la tabla con el resultado de la consulta 
                        conexion.Close();
                        return resultado; // retornamos una tabla con el resultado adjunto

                    }
                    catch (Exception ex)
                    {
                        // Manejar errores de conexión o SQL aquí.
                        conexion.Close();
                        return null;

                    }
                }
            }


        }

        public DataTable EliminarLogro(string campo)
        {

            DataTable Resulta = new DataTable();

            using (SqlConnection conexion = new SqlConnection(cadenaConexion))
            {
                string consulta = "DELETE FROM ListaLogros WHERE Logro = @LOGRO";
                conexion.Open();

                using (SqlCommand command = new SqlCommand(consulta, conexion))
                {
                    command.Parameters.AddWithValue("@LOGRO", campo);

                    SqlDataAdapter adaptador = new SqlDataAdapter(command);


                    try
                    {
                        adaptador.Fill(Resulta); // llenamos la tabla con el resultado de la consulta 
                        conexion.Close();
                        return Resulta; // retornamos una tabla con el resultado adjunto

                    }
                    catch (Exception ex)
                    {
                        // Manejar errores de conexión o SQL aquí.
                        conexion.Close();
                        return null;

                    }
                }

            }
        }


        public DataTable ModificarLogro(Logro Milogro, string LogroSeleccion)
        {

            DataTable Resulta = new DataTable();

            using (SqlConnection conexion = new SqlConnection(cadenaConexion))
            {
                string consulta = "UPDATE listaLogros  SET  " +
                    "Logro = @LOGRO, " +
                    "NombreJuego = @NOMBREJUEGO, " +
                    "Plataforma = @PLATAFORMA, " +
                    "Puntaje = @PUNTAJE," +
                    "Fecha = @FECHA" +
                    "WHERE Logro = @LOGROSELECCION;";
                conexion.Open();

                using (SqlCommand command = new SqlCommand(consulta, conexion))
                {
                    command.Parameters.AddWithValue("@LOGROSELECCION", LogroSeleccion);

                    command.Parameters.AddWithValue("@LOGRO", Milogro.LogroJuego);
                    command.Parameters.AddWithValue("@NOMBREJUEGO", Milogro.NombreJuego);
                    command.Parameters.AddWithValue("@PLATAFORMA", Milogro.Plataforma);
                    command.Parameters.AddWithValue("@PUNTAJE", Milogro.Puntaje);
                    command.Parameters.AddWithValue("@FECHA", Milogro.FechaDesbloqueo);

                    SqlDataAdapter adaptador = new SqlDataAdapter(command);


                    try
                    {
                        adaptador.Fill(Resulta); // llenamos la tabla con el resultado de la consulta 
                        conexion.Close();
                        return Resulta; // retornamos una tabla con el resultado adjunto

                    }
                    catch (Exception ex)
                    {
                        // Manejar errores de conexión o SQL aquí.
                        conexion.Close();
                        return null;

                    }
                }

            }



        }

    }
}

