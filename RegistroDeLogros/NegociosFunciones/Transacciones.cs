﻿using Persistencia;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Microsoft.VisualBasic;
using System.Security.Cryptography.X509Certificates;

namespace NegociosFunciones
{
    public class Transacciones
    {

    
     

     public SQLservicios ServiciosP = new SQLservicios();

    public Transacciones ()
        {
         
        }
     
     public DataTable VerTodosLosLogros()
        {

            DataTable result = new DataTable();
            result =  ServiciosP.VerTodosLogros();

            return result;
        }
     
     public DataTable EliminarLogro(string Logro)
        {
            DataTable result = new DataTable();
            result = ServiciosP.EliminarLogro(Logro);

            return result;
        }

     public DataTable ModificarLogro(Logro logro, string SeleccionLogro)
        {
            DataTable result = new DataTable();
            result = ServiciosP.ModificarLogro(logro, SeleccionLogro);

            return result;
        }

     public bool AgregarLogro(Logro OBJlogro)
        {
            

                try
                {

                    ServiciosP.AgregarLogro (OBJlogro);

                    return true;
                   
                }

                catch (Exception ex)
                {
                    ex = new Exception("Error no se logro agregar logro");
                    return false;
                }

        
        }

     public void AgregarJugador(Jugador OBJjugador)
        {


            try
            {

               // ServiciosP.AgregarJugador(OBJjugador);
                throw new Exception("Jugador agregado correctamente");
            }

            catch (Exception ex)
            {
                ex = new Exception("Error no se logro agregar jugador");

            }


        }

     public bool AgregarPlataforma(String Plataforma)
        {

            try
            {

                ServiciosP.AgregarPlataforma(Plataforma);
                return true;
                throw new Exception("Plataforma agregada correctamente");
            
            }

            catch (Exception ex)
            {
                ex = new Exception("Error no se logro agregar plataforma");
                
                return false;
            }
        }

     public List<Jugador> CargarJugadores(List<Jugador> ListJugador)
        {
         ServiciosP.LlenarListaJugadores (ListJugador);
         return ListJugador;
        }        

     public List<string> CargarPlataformas(List<string> ListPlataformas)
    {
          ServiciosP.LlenarListaPlataformas(ListPlataformas); 
          return ListPlataformas;
    }

     public DataTable BuscarxPlataforma(string Plataforma)
        {
            
           
           DataTable resultado = new DataTable();
           resultado = ServiciosP.BuscarPorPlataforma(Plataforma);
            
            return resultado;
        }

     public DataTable BuscarxNombre(string Nombre)
        {


            DataTable resultado = new DataTable();
            resultado = ServiciosP.BuscarPorNombre(Nombre);

            return resultado;
        }

     public DataTable Estadisticas()
        {
                DataTable resultado = new DataTable();
                resultado = ServiciosP.VerEstadisticas();
                return resultado;

            
        }

     public DataTable Detalles(string logro) 
        {

            DataTable resultado = new DataTable();
            resultado = ServiciosP.MostrarDetallesLogro(logro);
            return resultado;

        }

    }



    

    }

